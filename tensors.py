"""
   Tensor classes for elasticity problems
"""
#from dolfin import *
import numpy as np
import copy
import itertools

class RotationTensor:
    def __init__(self, euler_angles):
        phi_1=euler_angles[0]*np.pi/180.0
        phi=euler_angles[1]*np.pi/180.0
        phi_2=euler_angles[2]*np.pi/180.0
        c1=np.cos(phi_1)
        c2=np.cos(phi)
        c3=np.cos(phi_2)
        s1=np.sin(phi_1)
        s2=np.sin(phi)
        s3=np.sin(phi_2)
        self._coords=np.zeros(9)
        self._coords[0]= c1*c3-c2*s1*s3
        self._coords[3]=-c1*s3-c2*c3*s1
        self._coords[6]=s1*s2
        self._coords[1]=c3*s1+c1*c2*s3
        self._coords[4]=c1*c2*c3-s1*s3
        self._coords[7]=-c1*s2
        self._coords[2]=s2*s3
        self._coords[5]=c3*s2
        self._coords[8]=c2
    def __getitem__(self, key):
        i,j=key
        return self._coords[i*3+j]
        
class RankFourTensor:
    """general rank four tensor with addition and rotation"""
    def __init__(self):
        self._vals=np.zeros([3,3,3,3])
    def __getitem__(self,key):
        i,j,k,l=key
        return self._vals[i][j][k][l]
    def __add__(self,other):
        r=RankFourTensor()
        for i,j,k,l in itertools.product(range(3),repeat=4):
            r._vals[i][j][k][l]=self._vals[i][j][k][l]+other._vals[i][j][k][l]
        return r
    def rotate(self,R):
        _o_vals=copy.deepcopy(self._vals)
        for i,j,k,l in itertools.product(range(3),repeat=4):
            temp=0.0
            for m,n,o,p in itertools.product(range(3),repeat=4):
                temp=temp+R[i,m]*R[j,n]*R[k,o]*R[l,p]*_o_vals[m][n][o][p]
        self._vals[i][j][k][l]=temp
    def array(self):
        """return numpy array """
        return self._vals
    def tensor(self):
        """return Dolfin UFL tensor """
        return as_tensor(self._vals)
    def pprint(self,style="full"):
        """pretty print for debug purpose"""
        if style=="full":
            for i,j in itertools.product(range(3),repeat=2):
                print "i="+str(i)+" j="+str(j)
                for k,l in itertools.product(range(3),repeat=2):
                    print "("+str(k)+","+str(l)+"):"+str(self[i,j,k,l]),
                print " "
        else:
            if style=="voigt":
                voigtIndex=((0,0), (1,1), (2,2), (1,2), (0,2), (0,1));
                for i,j in voigtIndex:
                    for k,l in voigtIndex:
                        print "{:^12.2e}".format(self[i,j,k,l]),
                    print " "
            else:
                if style=="9":
                    voigtIndex=((0,0), (1,1), (2,2), (1,2), (0,2), (0,1));
                    for m,n in ((0,0),(0,1),(0,2),(1,1),(1,2),(2,2),(3,3),(4,4),(5,5)):
                        i,j=voigtIndex[m]
                        k,l=voigtIndex[n]
                        print "{:^12.2e}".format(self[i,j,k,l]),
                    print " "
                else:
                    raise RuntimeError("unknown style")
             

class ElasticityTensor(RankFourTensor):
    """
    C_ijkl elasticity rank four tensor. 
    """
    def __init__(self,*args,**kwargs):
        RankFourTensor.__init__(self)
        if "vector" in kwargs:
            v=kwargs["vector"]
            assert ( v.size==9 or v.size==21), "Input vector must be of size 9 (for orthotropy) of size 21!"
            if v.size==9:
                # for orthotropic material 
                self._vals[0][0][0][0]=v[0]
                self._vals[0][0][1][1]=v[1]
                self._vals[0][0][2][2]=v[2]
                self._vals[1][1][1][1]=v[3]
                self._vals[1][1][2][2]=v[4]
                self._vals[2][2][2][2]=v[5]
                self._vals[1][2][1][2]=v[6]
                self._vals[0][2][0][2]=v[7]
                self._vals[0][1][0][1]=v[8]
            else:
                # for general anisotropic material
                self._vals[0][0][0][0] = input[0]; #C1111
                self._vals[0][0][1][1] = input[1]; #C1122
                self._vals[0][0][2][2] = input[2]; #C1133
                self._vals[0][0][1][2] = input[3]; #C1123
                self._vals[0][0][0][2] = input[4]; #C1113
                self._vals[0][0][0][1] = input[5]; #C1112

                self._vals[1][1][1][1] = input[6]; #C2222
                self._vals[1][1][2][2] = input[7]; #C2233
                self._vals[1][1][1][2] = input[8]; #C2223
                self._vals[0][2][1][1] = input[9]; #C2213  //flipped for filling purposes
                self._vals[0][1][1][1] = input[10]; #C2212 //flipped for filling purposes

                self._vals[2][2][2][2] = input[11]; #C3333
                self._vals[1][2][2][2] = input[12]; #C3323 //flipped for filling purposes
                self._vals[0][2][2][2] = input[13]; #C3313 //flipped for filling purposes
                self._vals[0][1][2][2] = input[14]; #C3312 //flipped for filling purposes
    
                self._vals[1][2][1][2] = input[15]; #C2323
                self._vals[0][2][1][2] = input[16]; #C2313 //flipped for filling purposes
                self._vals[0][1][1][2] = input[17]; #C2312 //flipped for filling purposes
     
                self._vals[0][2][0][2] = input[18]; #C1313
                self._vals[0][1][0][2] = input[19]; #C1312 //flipped for filling purposesc
    
                self._vals[0][1][0][1] = input[20]; #C1212
                
            for i,j,k,l in itertools.product(range(3),repeat=4):
                self._vals[i][j][l][k]=self._vals[i][j][k][l]
                self._vals[j][i][k][l]=self._vals[i][j][k][l]
                self._vals[j][i][l][k]=self._vals[i][j][k][l]
                self._vals[k][l][i][j]=self._vals[i][j][k][l]
                self._vals[l][k][j][i]=self._vals[i][j][k][l]
                self._vals[k][l][j][i]=self._vals[i][j][k][l]
                self._vals[l][k][i][j]=self._vals[i][j][k][l]
        else:
            assert ("E" in kwargs and "nu" in kwargs), \
                "Please specify E(Young's Module) and nu(poisson ration) for isotropic material!\n \
                  For anisotropic material, specfic a list of size 9 or size 21."
            if "E" in kwargs and "nu" in kwargs:
                E=kwargs["E"]
                nu=kwargs["nu"]
                mu= E/(2.0*(1.0 + nu))
                lmbda=E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
                for i,j,k,l in itertools.product(range(3),repeat=4):
                    self._vals[i][j][k][l]=mu*(_kd(i,k)*_kd(j,l)+_kd(i,l)*_kd(k,j))+lmbda*_kd(i,j)*_kd(k,l)

                    
def _kd(i,j):          #only work for python 2.5+
    return 1 if (i==j) else 0
            
