#!/usr/bin/python
"""
Parse the vtk files from elasticity solvers. Merge the corresponding displacement, strain and stress into one singe file.
The merged file can then be visualized in Paraview. 
"""
import sys, itertools
from xml.dom import minidom

if len(sys.argv)<4:
    print "Usage: merge_output <directory>  <number of eigenmode> <number of timestep>"
    print "Example: merge_output twosheet_output 2 128"
    sys.exit(2)
    
dir_name=sys.argv[1]+'/'

#total number of eigenmode
n1=int(sys.argv[2])

#total number of files for each eigen model
n2=int(sys.argv[3])

for i in range(n1):
    for j in range(n2):
        fileName_displacement=dir_name+"displacement_No%d_%06d.vtu" % (i,j)
        fileName_stress=dir_name+"stress_No%d_%06d.vtu" % (i,j)
        fileName_strain=dir_name+"strain_No%d_%06d.vtu" % (i,j)
        fileName_out=dir_name+"all_No%d_%06d.vtu" % (i,j)
        doc_displacement=minidom.parse(fileName_displacement)
        doc_stress=minidom.parse(fileName_stress)
        doc_strain=minidom.parse(fileName_strain)   
        pointdata_displacement=doc_displacement.getElementsByTagName("PointData")[0]
        pointdata_stress=doc_stress.getElementsByTagName("PointData")[0]
        pointdata_strain=doc_strain.getElementsByTagName("PointData")[0]
        stress=pointdata_stress.getElementsByTagName("DataArray")[0]
        strain=pointdata_strain.getElementsByTagName("DataArray")[0]
        pointdata_displacement.setAttribute("Tensors","stress strain")
        pointdata_displacement.appendChild(stress)
        pointdata_displacement.appendChild(strain)
        outfile=open(fileName_out,"w")
        doc_displacement.writexml(outfile)
        outfile.close()
    #merge all all timesteps into one paraview file
    fin=open(dir_name + "displacement_No%d_.pvd" % i)
    fout=open(dir_name + "all_No%d_.pvd" % i,"w")
    line=fin.readline()
    while line:
        fout.write(line.replace("displacement","all"))
        line=fin.readline()
    fin.close()
    fout.close()
    
