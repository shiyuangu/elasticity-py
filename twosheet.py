"""
Compute the resonance frequencies of a PVDF-Steel sheet.
PVDF-Steel: 7.5cm by 2 cm by 13 um (PVDF:5um; Steel: 8um)
PVDF part: Young's Modulus: 3GPa; Poisson ratio:0.18; Density: 4062kg/m^3;
Steel Part: Young's Modulus: 200GPa; Poisson ratio: 0.29; Density: 8000kg/m^3
"""
from dolfin import *
import numpy as np
from tensors import *
from helpers import *
import os
import pdb

name="twosheet"
length=7.5e4
width=2e4
height=13
divider=8  # steel: 8 micrometer

nx=200
ny=40
nz=4

ne=3 #number of requested eigenvalues

mesh=BoxMesh(0.0,0.0,0.0,length,width,height,nx,ny,nz)

#output directory
dir_name=name+"_output"
#create the ouptut directory if not exist
if not os.path.isdir(dir_name):
    os.mkdir(dir_name)

def elasticity_eigsolver(C,rho,V,n=1,bc=None,subdomains=None):
    """
    C:   a ElasticityTensorn list of size of 
    rho: mass density
    n:   number of requsted eigen values
    V:   the finite element space
    bc:  boundary condition
    subdomains: a CellFunction which marks the domain
    """
    
    u=TrialFunction(V)
    v=TestFunction(V)
    dx=Measure("dx")[subdomains]
    i,j,k,l=indices(4)
    a=C[0].tensor()[i,j,k,l]*grad(u)[k,l]*grad(v)[i,j]*dx(0)+C[1].tensor()[i,j,k,l]*grad(u)[k,l]*grad(v)[i,j]*dx(1)
        
    A=PETScMatrix()
    M=PETScMatrix()
    a_mass=rho[0]*dot(u,v)*dx(0)+rho[1]*dot(u,v)*dx(1)
    
    f=Constant((0.0,0.0,0.0));
    L=inner(f,v)*dx()
    assemble_system(a_mass,L,bc,A_tensor=M)
    assemble_system(a,L,bcl,A_tensor=A)
    
    #set up eigen solver 
    eigensolver=SLEPcEigenSolver(A,M)
    eigensolver.parameters["spectrum"]="smallest magnitude"
    eigensolver.parameters["solver"]="arnoldi"
    eigensolver.parameters["spectral_transform"]="shift-and-invert"
    eigensolver.parameters["spectral_shift"]=0.0
    eigensolver.parameters["tolerance"]=1e-8
    eigensolver.parameters["maximum_iterations"]=1000000000
    eigensolver.parameters["problem_type"]="gen_hermitian"
    print "Solving eigen value..."
    eigensolver.solve(n)
    print "Eigen solve completed"
    eigenpairs=[];
    for i in range(n):
        ev=Function(V)
        r,c,rx,cx=eigensolver.get_eigenpair(i)
        print "{}th smallest eigenvalue: {}".format(i,r)
        ev.vector()[:]=rx
        eigenpairs.append((r,ev)) 
    return eigenpairs
    
def left(x, on_boundary):
    flag=on_boundary and x[0]<=0.0*(1.0-DOLFIN_EPS) and x[0]>=0.0*(1+DOLFIN_EPS)
    return flag
 
def right(x, on_boundary):
    flag=on_boundary and x[0]<=length*(1.0+DOLFIN_EPS) and x[0]>=length*(1-DOLFIN_EPS)
    return flag

class SteelPart(SubDomain):
    def inside(self, x, on_boundary):
        return x[2]<divider
        
class PVDFPart(SubDomain):
    def inside(self, x, on_boundary):
        return x[2]>=divider

#set up C_ijkl for PVDF and Steel
R=RotationTensor(np.array([0.0, 0.0, 0.0]))
C_Steel=ElasticityTensor(vector=np.array([262.07, 107.05, 107.05, 262.07, 107.05, 262.07, 77.51, 77.51, 77.51]))
C_Steel.rotate(R)
C_PVDF=ElasticityTensor(vector=np.array([3.25, 0.71, 0.71, 3.25, 0.71, 3.25, 1.27, 1.27, 1.27]))
C_PVDF.rotate(R)
C=[C_Steel,C_PVDF]
rho=[8.0,4.062]

#set up subdomains
subdomains=CellFunction('uint',mesh)
steel_part=SteelPart()
steel_part.mark(subdomains,0)
PVDF_part=PVDFPart()
PVDF_part.mark(subdomains,1)

V = VectorFunctionSpace(mesh, "CG", 1)
bcl = DirichletBC(V, Constant((0.0, 0.0, 0.0)), left)


eigenpairs=elasticity_eigsolver(C,rho,V,ne,bcl,subdomains)

#compute frequcies using the eigen values 
##compute the scaling factor
len_scale=1e12      #scaling argument and the 1micrometer=1e-6 meter
C_scale=1e9         #1GPa = 1e9 Pa
rho_scale=1e3       #unit of the mass density in kg/m^3

scaling_factor=len_scale*C_scale/rho_scale #scaling factor to the frequency
for i in range(len(eigenpairs)):
    lmbda=eigenpairs[i][0]
    omega=sqrt(lmbda*scaling_factor)
    freq=omega/(2*pi)
    print "{}th smallest frequency: {}kHz".format(i,freq/1e3)

#generate vibration modes and output to files.
#caution:this is very slow. 
for i in range(len(eigenpairs)):
    file_displacement=File(dir_name+"/displacement_No"+str(i)+"_.pvd","compressed")
    file_stress=File(dir_name+"/stress_No"+str(i)+"_.pvd","compressed")
    file_strain=File(dir_name+"/strain_No"+str(i)+"_.pvd","compressed")
    lmbda, ev=eigenpairs[i]
    strain,stress=compute_strain_stress(ev,C,mesh,subdomains)
    
    #generate time sequences for vibration
    omega=sqrt(lmbda*scaling_factor)
    sample_size=128
    ev_t=Function(V)
    strain_t=Function(TensorFunctionSpace(mesh,"CG",1))
    stress_t=Function(TensorFunctionSpace(mesh,"CG",1))
    for j in range(sample_size):
        t=2*pi/omega*j/sample_size
        ev_t.vector()[:]=ev.vector()*sin(omega*t)
        strain_t.vector()[:]=strain.vector()*sin(omega*t)
        stress_t.vector()[:]=stress.vector()*sin(omega*t)
        #print "Norm:", np.linalg.norm(ev_t[j].vector().array(), ord=np.Inf)
        file_displacement<<(ev_t,t)
        file_strain<<(strain,t)
        file_stress<<(stress,t)


