"""
  Helper functions for elasticity problems
  Author: S. Gu 
"""
_all__ = ['compute_strain_stress']
from dolfin import *
from tensors import *
def compute_strain_stress(u,C,mesh,subdomains):
    """
    return a ufl TensorFuntion strain and stress;
    This is a modification of the project function in dolfin.
    *Argument*
        u: displacement vector function
        C: a list of ElasticityTensor
        mesh: FIXME: no need to pass in mesh; mesh should be extracted from u
        subdomains: A CellFunction representing the partition of subdomains
    """ 
    if not (type(C) is list):
        raise TypeError, "C should be a list of ElasticityTensor"
    else:
        for i in range(len(C)):
            if not isinstance(C[i],ElasticityTensor):
                raise TypeError, "C[{}] should be a ElasticityTensor".format(i)

    V=TensorFunctionSpace(mesh,'Lagrange',1)
    #compute strain
    strain_tmp=0.5*(grad(u)+grad(u).T)
    strain=project(strain_tmp,V)
    strain.rename("strain","strain")
    
    #compute stress
    dx=Measure("dx")[subdomains]
    i,j,k,l=indices(4)
    tw=TestFunction(V)
    tv=TrialFunction(V)
    L=C[0].tensor()[i,j,k,l]*strain_tmp[k,l]*tw[i,j]*dx(0)
    for idx in range(1,len(C)):
        L=L+C[idx].tensor()[i,j,k,l]*strain_tmp[k,l]*tw[i,j]*dx(idx)
    a=inner(tw,tv)*dx()
    stress=Function(V)
    solve(a==L,stress)
    stress.rename("stress","stress")
    
    return (strain,stress)
    
    

